=========
AuditUUID
=========

------------------------------------------------------
Generate unique patient identifiers for clinical audit
------------------------------------------------------

Introduction
------------
When carrying out a clinical audit it is usually necessary to anonymise data
that may be seen by a third party who should not have access to patient 
identifiable confidential information. To that end when tabulating patient 
data for the audit each patient should only be identified by a unique code. 
**AuditUUID** is a tool to generate such codes.

Requirements
------------

For audit validation a sample of the audit data may need to be checked by 
someone other than the primary auditor. For this to be possible the primary 
auditor will need to maintain a database back-mapping the codes to the original 
patients. In case the database should be corrupted, maliciously altered or 
lost it is necessary that the codes should be:

- reproducibly generated for each patient
- unique within the patient group
- not be trivially mathematically reversible to identify the patient

so they can be regenerated if required. In addition it is helpful if the codes 
are:

- short to avoid errors of transcription
- short to allow quick comparison of codes

Implementation
--------------

I looked at using one of the common cryptographic hashes to generate the 
codes. While they do an excellent job of meeting the first three requirements 
they are all quite long - usually 32 or 64 hex or decimal digits. This 
introduces a high risk of transcription errors.

In the end I used the Java hashing algorithm as suggested here_ using 541 as
the prime seed so the hashes would be 100% unique in a 7-bit ascii world. As I
am implementing ``audituuid`` in Python I don't need to worry about whether the
hash result is too large for an int. The interpreter takes care of this,
switching to a bignum if required. The code is the hexadecimal form of the
hash.

.. _here: https://stackoverflow.com/a/12571496

The information you hash must be unique within your patient set. For example 
I have used patient first and last names, date of birth and the date of 
examination being audited. Bigger data sets may need more data points in the 
hashed data to avoid duplication.
