# -*- coding: utf-8 -*-
"""
Main setup config for PyPi packaging
"""

########################################################################
# imports
########################################################################
# standard library imports
from setuptools import setup


# standard library imports
from audituuid.version import get_package_version_str


########################################################################
# Atributes
########################################################################
__author__ = "Erehwon Tools <devops@erehwon.xyz>"
__copyright__ = "Copyright (c) 2004-2019 Erehwon Tools"
__license__ = "New-style BSD"
__version__ = get_package_version_str()


########################################################################
# Interface
########################################################################


########################################################################
# Classes
########################################################################


########################################################################
# Functions
########################################################################


def readme():
    with open('DESCRIPTION.rst') as f:
        return f.read()


########################################################################
# Entry point
########################################################################


setup(name='audituuid',
      version=get_package_version_str(),
      description="audituuid is a tool to generate short(ish) unique \
        identifiers to anonymize subjects in a clinical audit.",
      long_description=readme(),
      classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience  :: Healthcare Industry',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 2.7',
        'Operating System :: OS Independent',
        'Topic :: Scientific/Engineering :: Medical Science Apps.'
      ],
      keywords='audituuid clinical audit medical dental medicine dentistry \
        anonymize uuid hash',
      url='https://bitbucket.org/apollonia/audituuid',
      author='Erehwon Tools',
      author_email='devops@erehwon.xyz',
      license='New BSD',
      download_url='https://bitbucket.org/apollonia/audituuid/\
        get/master.tar.gz',
      packages=['audituuid'],
      entry_points={
        'console_scripts': [
            'audituuid = audituuid.cli:main_tool'
        ]
      },
      include_package_data=True,
      zip_safe=False)
