# -*- coding: utf-8 -*-
"""
Holds 'console_scripts' entry points for package
"""


########################################################################
# Package/Module imports
########################################################################
# standard library imports
from __future__ import print_function
import sys
import os
import argparse
from argparse import ArgumentParser


# package specific imports
from .version import get_package_version_str
from .util import get_script_name


########################################################################
# Atributes
########################################################################
__author__ = "Erehwon Tools <devops@erehwon.xyz>"
__copyright__ = "Copyright (c) 2004-2019 Erehwon Tools"
__license__ = "New-style BSD"
__version__ = get_package_version_str()


########################################################################
# Interface
########################################################################
__all__ = [
            # Functions
            'main_tool'
            ]


########################################################################
# Constants
########################################################################
FILE_HELP_TEXT = \
    """
    File to process.

    The file should contain one line for each patient. Each line should hold
    enough data to identify the patient and the clinical event being audited.
    The lines should be newline terminated.
    """


########################################################################
# Classes
########################################################################


########################################################################
# Functions
########################################################################

def my_hash(inString):
    h = 0
    for elem in inString:
        h = 541 * h + ord(elem)
    return h


def main_tool():
    # parse options and args
    parser = ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description="AuditUUID")

    parser.add_argument('-v', '--version', action='version',
                        version="%(prog)s ("+__version__+")")

    parser.add_argument(
        "file_name", metavar='FILE_NAME', type=str, nargs=1,
        help=FILE_HELP_TEXT)

    args = parser.parse_args()

    if args.file_name is None:
        error_msg = get_script_name() + ": Error: AuditUUID requires a file "\
            "of patient data to act on. Exiting..."
        print(error_msg, file=sys.stderr, flush=True)
        sys.exit(1)

    if os.path.exists(args.file_name) is False:
        error_msg = get_script_name() + ": Error: The file '" + \
            args.file_name + "' does not exist. Exiting..."
        print(error_msg, file=sys.stderr, flush=True)
        sys.exit(1)

    if os.path.isfile(args.file_name) is False:
        error_msg = get_script_name() + ": Error: '" + args.file_name + \
            "' is not a file. Exiting..."
        print(error_msg, file=sys.stderr, flush=True)
        sys.exit(1)

    with open(args.file_name, 'r') as patient_file:
        for line in patient_file:
            print(line + '\t' + my_hash(line))
