# -*- coding: utf-8 -*-
"""
Utility routines.
"""

# standard library imports
import os


# third party imports


# package specific imports
import __main__ as main
from version import get_package_version_str

########################################################################
# Atributes
########################################################################
__author__ = "Erehwon Tools <devops@erehwon.xyz>"
__copyright__ = "Copyright (c) 2004-2019 Erehwon Tools"
__license__ = "New-style BSD"
__version__ = get_package_version_str()


########################################################################
# Interface
########################################################################
__all__ = [
            # Functions
            'get_script_name'
            ]


########################################################################
# Classes
########################################################################


########################################################################
# Functions
########################################################################

def get_script_name():
    """Returns the stripped name of the script file that called the function"""
    s_name = os.path.basename(main.__file__)
    return s_name
