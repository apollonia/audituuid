=========
AuditUUID
=========

**AuditUUID** is a tool to generate short(ish) unique identifiers to anonymize 
subjects in a clinical audit.

Installation and Set-up
=======================

The best way to install AuditUUID is via PyPi and pip.

Install with:

	``pip install audituuid``

and the ``audituuid`` command should be placed somewhere in your path. On some
systems/installations you may need a:

	``sudo pip install audituuid``.

instead.

Input Data
==========

The data to be hashed will be read from a file. The data for each individual
patient should be on a line. The lines should be separated by ``newline``. The
data chosen to hash should be of adequate quantity and variety that it will be
unique for each patient. In small patient datasets something as simple as first
name, surname and date of birth may be adequate but larger datasets will
require further per-patient data to avoid input data collisions.

Usage
=====

Get help with ``audituuid -h`` or ``audituuid --help``

Get the current version with ``audituuid -v`` or ``audituuid --version``

Invoke audituuid with the following command: ``audituuid FILE_NAME`` where 
FILE_NAME is the path to a file with the patient data to be anonymised.

Contribution guidelines
=======================

Feel free to send me pull requests. I have one requirement, all submitted
code must pass checking with flake8_

.. _flake8: http://flake8.pycqa.org

Licence
=======

New-Style BSD

Footnotes
=========

.. target-notes::
